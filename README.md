# Netprise

Netprise Assignment

#Dependencies:
1. NodeJS - download the file from nodejs.org
2. Angular Cli - after installing nodejs, run the command `npm i @angular/cli`
3. MongoDB - download the file from https://www.mongodb.com/

#Database Setup:
1. Create directories 'data/db' at the root for MongoDB to store the data.
2. Run the mongod.exe and mongo.exe in separate cmd.
3. Create a database 'netprise' - use netprise 
4. Create two collections 'users'  and 'appointments' - db.createCollection('users') and db.createCollection('appointments')
5. Insert a default user in the 'users' collection - db.users.insert({ username: 'Admin', password: '123' })

#Backend:
1. Extract the contents of the Server.zip file into a directory.
2. Then from the root directory use CMD to run `npm install` to install all the dependencies
3. Once the dependencies are installed, in the CMD type `npm start`


#Frontend :
1. Extract the contents of the Frontend.zip file into a directory.
2. Then from the root directory use CMD to run `npm install` to install all the dependencies
3. Once the dependencies are installed, in the CMD type `ng serve -o` to open the application in the browser